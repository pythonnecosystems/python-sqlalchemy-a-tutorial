# Python SQLAlchemy: A Tutorial

## Python SQLAlchemy 사용법

### 패키지 설치

```bash
$ pip install sqlalchemy
```

### 데이터베이스 연결
데이터베이스와 상호 작용을 시작하려면 먼저 연결을 설정해야 한다.

```python
import sqlalchemy as db

engine = db.create_engine('dialect+driver://user:pass@host:port/db')
```

### 테이블 세부 정보 보기
SQLAlchemy는 리플렉션을 사용하여 데이터베이스에서 테이블을 자동으로 로드하는 데 사용할 수 있다. 리플렉션은 데이터베이스를 읽고 해당 정보를 기반으로 메타데이터를 작성하는 프로세스이다.

예를 들어,

```python
import sqlalchemy as db

engine = db.create_engine('sqlite:///census.sqlite')
connection = engine.connect()
metadata = db.MetaData()
census = db.Table('census', metadata, autoload=True, autoload_with=engine)

# Print the column names
print(census.columns.keys())

['state', 'sex', 'age', 'pop2000', 'pop2008']

# Print full table metadata
print(repr(metadata.tables['census']))

Table('census', MetaData(bind=None), Column('state', VARCHAR(length=30), table=), Column('sex', VARCHAR(length=1), table=), Column('age', INTEGER(), table=), Column('pop2000', INTEGER(), table=), Column('pop2008', INTEGER(), table=), schema=None)
```

## 쿼리
`Table`과 `MetaData`를 이미 임포트했다. 메타데이터를 `metadata`로 사용할 수 있다.

```python
import sqlalchemy as db

engine = db.create_engine('sqlite:///census.sqlite')
connection = engine.connect()
metadata = db.MetaData()
census = db.Table('census', metadata, autoload=True, autoload_with=engine)

#Equivalent to 'SELECT * FROM census'
query = db.select([census]) 

ResultProxy = connection.execute(query)

ResultSet = ResultProxy.fetchall()

ResultSet[:3]

[('Illinois', 'M', 0, 89600, 95012),
 ('Illinois', 'M', 1, 88445, 91829),
 ('Illinois', 'M', 2, 88729, 89547)]
```

다음과 같은 두 함수를 알아두자.

- **ResultProxy**: `.execute()` 메서드가 반환하는 객체이다. 쿼리를 통해 데이터를 반환하는 다양한 방법으로 사용할 수 있다.
- **ResultSet**: ResultProxy에서 `.fetchall()`같은 fetch 메서드를 사용할 때 쿼리에서 요청된 실제 데이터이다.

## 대규모 결과 집합 처리
대규모 데이터 세트의 경우 최적의 레코드 수를 로드하고 메모리 문제를 극복하기 위해 `.fetchmany()`를 사용한다.

```python
while flag:
    partial_results = ResultProxy.fetchmany(50)
    if(partial_results == []): 
	    flag = False
    //
	code
   //
ResultProxy.close()
```

## 데이터프레임으로 변환

```python
df = pd.DataFrame(ResultSet)
df.columns = ResultSet[0].keys()
```

## Python SQLAlchemy로 데이터를 필터링하는 방법
SQLite 쿼리의 몇 가지 예와 SQLAlchemy를 사용한 쿼리를 살펴보겠다.

**Python SQLAlchemy로 데이터를 필터링하기 위해 알아야 할 중요 함수**
- Where
- In
- And, or, not
- Order by
- Avg, count, min, max
- Group by
- Distinct
- Case and cast
- Joins

### WHERE
SQL :
```sql
SELECT * FROM census
WHERE sex = "F"
```

SQLAlchemy :
```python
db.select([census]).where(census.columns.sex == 'F')
```

### IN
SQL :
```sql
SELECT state, sex
FROM census
WHERE state IN (Texas, New York)
```

SQLAlchemy :
```python
db.select([census.columns.state, census.columns.sex]).where(census.columns.state.in_(['Texas', 'New York']))
```

### AND, OR, NOT
SQL :
```sql
SELECT * FROM census
WHERE state = 'California' AND NOT sex = 'M'
```

SQLAlchemy :
```python
db.select([census]).where(db.and_(census.columns.state == 'California', census.columns.sex != 'M'))
```

### ORDER BY
SQL :
```sql
SELECT * FROM census
ORDER BY State DESC, pop2000
```

SQLAlchemy :
```python
db.select([census]).order_by(db.desc(census.columns.state), census.columns.pop2000)
```

## Python SQLAlchemy를 위해 알아야 할 중요한 함수

SQL :
```sql
SELECT SUM(pop2008)
FROM census
```

SQLAlchemy :
```python
db.select([db.func.sum(census.columns.pop2008)])
```

다른 함수으로는 `avg`, `count`, `min` 및 `max`가 있다.

### GROUP BY
SQL :
```sql
SELECT SUM(pop2008) as pop2008, sex
FROM census
GROUP BY sex
```

SQLAlchemy :
```python
db.select([db.func.sum(census.columns.pop2008).label('pop2008'), census.columns.sex]).group_by(census.columns.sex)
```

### DISTINCT
SQL :
```sql
SELECT DISTINCT state
FROM census
```

SQLAlchemy :
```python
db.select([census.columns.state.distinct()])
```

### CASE와 CAST
`case()` 표현식은 일치시킬 조건 목록과 조건이 일치할 경우 반환할 열을 받고, 조건이 일치하지 않으면 `else_`를 받는다. 표현식을 특정 유형으로 변환하려면 ₩ 함수를 사용한다.

예를 들면 다음과 같다.

```python
import sqlalchemy as db

engine = db.create_engine('sqlite:///census.sqlite')
connection = engine.connect()
connection = engine.connect()
metadata = db.MetaData()
census = db.Table('census', metadata, autoload=True, autoload_with=engine)

female_pop = db.func.sum(db.case([(census.columns.sex == 'F', census.columns.pop2000)],else_=0))

total_pop = db.cast(db.func.sum(census.columns.pop2000), db.Float)

query = db.select([female_pop/total_pop * 100])

result = connection.execute(query).scalar()
print(result)

51.09467432293413
```

결과에 단일 값만 포함된 경우 결과에 `.scalar`를 사용한다.

### JOINS
이미 관계가 설정된 두 테이블이 있는 경우 각 테이블에서 원하는 열을 `select` 문에 추가하기만 하면 해당 관계를 자동으로 사용할 수 있다. 또는 수동으로 할 수도 있다.

```python
select([census.columns.pop2008, state_fact.columns.abbreviation])
```

예를 들어 데이터베이스 가져오기부터 시작해 본다.

```python
import sqlalchemy as db
import pandas as pd

engine = db.create_engine('sqlite:///census.sqlite')
connection = engine.connect()
metadata = db.MetaData()

census = db.Table('census', metadata, autoload=True, autoload_with=engine)
state_fact = db.Table('state_fact', metadata, autoload=True, autoload_with=engine) 
```

#### 자동 Join
```python
#Automatic Join
query = db.select([census.columns.pop2008, state_fact.columns.abbreviation])
result = connection.execute(query).fetchall()
df = pd.DataFrame(results)
df.columns = results[0].keys()
df.head(5)
```

```
state	sex	age	pop2000	pop2008	id	name	abbreviation	country	type	...	occupied	notes	fips_state	assoc_press	standard_federal_region	census_region	census_region_name	census_division	census_division_name	circuit_court
0	Illinois	M	0	89600	95012	13	Illinois	IL	USA	state	...	occupied		17	Ill.	V	2	Midwest	3	East North Central	7
1	Illinois	M	1	88445	91829	13	Illinois	IL	USA	state	...	occupied		17	Ill.	V	2	Midwest	3	East North Central	7
2	Illinois	M	2	88729	89547	13	Illinois	IL	USA	state	...	occupied		17	Ill.	V	2	Midwest	3	East North Central	7
3	Illinois	M	3	88868	90037	13	Illinois	IL	USA	state	...	occupied		17	Ill.	V	2	Midwest	3	East North Central	7
4	Illinois	M	4	91947	91111	13	Illinois	IL	USA	state	...	occupied		17	Ill.	V	2	Midwest	3	East North Central	7
5 rows × 22 columns
```

#### 수동 Join
```python
#Manual Join
query = db.select([census, state_fact])
query = query.select_from(census.join(state_fact, census.columns.state == state_fact.columns.name))
results = connection.execute(query).fetchall()
df = pd.DataFrame(results)
df.columns = results[0].keys()
df.head(5)
```

```
state	sex	age	pop2000	pop2008	id	name	abbreviation	country	type	...	occupied	notes	fips_state	assoc_press	standard_federal_region	census_region	census_region_name	census_division	census_division_name	circuit_court
0	Illinois	M	0	89600	95012	13	Illinois	IL	USA	state	...	occupied		17	Ill.	V	2	Midwest	3	East North Central	7
1	Illinois	M	1	88445	91829	13	Illinois	IL	USA	state	...	occupied		17	Ill.	V	2	Midwest	3	East North Central	7
2	Illinois	M	2	88729	89547	13	Illinois	IL	USA	state	...	occupied		17	Ill.	V	2	Midwest	3	East North Central	7
3	Illinois	M	3	88868	90037	13	Illinois	IL	USA	state	...	occupied		17	Ill.	V	2	Midwest	3	East North Central	7
4	Illinois	M	4	91947	91111	13	Illinois	IL	USA	state	...	occupied		17	Ill.	V	2	Midwest	3	East North Central	7
5 rows × 22 columns
```

## Python SQLAlchemy로 테이블을 만들고 테이블에 데이터를 삽입하는 방법
존재하지 않는 데이터베이스를 엔진에 전달하면 SQLAlchemy가 자동으로 새 데이터베이스를 생성한다.

```python
import sqlalchemy as db
import pandas as pd

#Creating Database and Table
engine = db.create_engine('sqlite:///test.sqlite') #Create test.sqlite automatically
connection = engine.connect()
metadata = db.MetaData()

emp = db.Table('emp', metadata,
              db.Column('Id', db.Integer()),
              db.Column('name', db.String(255), nullable=False),
              db.Column('salary', db.Float(), default=100.0),
              db.Column('active', db.Boolean(), default=True)
              )

metadata.create_all(engine) #Creates the table

#Inserting Data
#Inserting record one by one
query = db.insert(emp).values(Id=1, name='naveen', salary=60000.00, active=True) 
ResultProxy = connection.execute(query)

#Inserting many records at ones
query = db.insert(emp) 
values_list = [{'Id':'2', 'name':'ram', 'salary':80000, 'active':False},
               {'Id':'3', 'name':'ramesh', 'salary':70000, 'active':True}]
ResultProxy = connection.execute(query,values_list)

results = connection.execute(db.select([emp])).fetchall()
df = pd.DataFrame(results)
df.columns = results[0].keys()
df.head(4)
```

```
    Id	name	salary	active
0	1	vinay	60000.0	True
1	1	satvik	60000.0	True
2	1	naveen	60000.0	True
3	2	rahul	80000.0	False
```

### 데이터베이스의 데이터 업데이트

```python
db.update(table_name).values(attribute = new_value).where(condition)
```

```python
import sqlalchemy as db
import pandas as pd

engine = db.create_engine('sqlite:///test.sqlite')
metadata = db.MetaData()
connection = engine.connect()
emp = db.Table('emp', metadata, autoload=True, autoload_with=engine)

results = connection.execute(db.select([emp])).fetchall()
df = pd.DataFrame(results)
df.columns = results[0].keys()
df.head(4)
```

```
    Id	name	salary	active
0	1	vinay	60000.0	True
1	1	satvik	60000.0	True
2	1	naveen	60000.0	True
3	2	rahul	80000.0	False
```

```python
# Build a statement to update the salary to 100000
query = db.update(emp).values(salary = 100000)
query = query.where(emp.columns.Id == 1)
results = connection.execute(query)

results = connection.execute(db.select([emp])).fetchall()
df = pd.DataFrame(results)
df.columns = results[0].keys()
df.head(4)
```
```
    Id	name	salary	active
0	1	vinay	100000.0	True
1	1	satvik	100000.0	True
2	1	naveen	100000.0	True
3	2	rahul	80000.0	False
```

### 테이블에 레코드 삭제
```python
db.delete(table_name).where(condition)
```

```python
import sqlalchemy as db
import pandas as pd

engine = db.create_engine('sqlite:///test.sqlite')
metadata = db.MetaData()
connection = engine.connect()
emp = db.Table('emp', metadata, autoload=True, autoload_with=engine)

results = connection.execute(db.select([emp])).fetchall()
df = pd.DataFrame(results)
df.columns = results[0].keys()
df.head(4)
```
```
Id	name	salary	active
0	1	vinay	100000.0	True
1	1	satvik	100000.0	True
2	1	naveen	100000.0	True
3	2	rahul	80000.0	    False
```
```python
# Build a statement to delete where salary < 100000
query = db.delete(emp)
query = query.where(emp.columns.salary < 100000)
results = connection.execute(query)

results = connection.execute(db.select([emp])).fetchall()
df = pd.DataFrame(results)
df.columns = results[0].keys()
df.head(4)
```
```
	Id	name	salary	active
0	1	vinay	100000.0	True
1	1	satvik	100000.0	True
2	1	naveen	100000.0	True
```

### 테이블 삭제
```python
table_name.drop(engine) #drops a single table
metadata.drop_all(engine) #drops all the tables in the database
```
