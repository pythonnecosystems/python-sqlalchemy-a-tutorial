# Python SQLAlchemy: A Tutorial <sup>[1](#footnote_1)</sup>

> Python SQLAlchemy는 관계형 데이터베이스와 상호 작용을 Python 방식으로 제공하며 워크플로우를 간소화하는 데 도움을 줄 수 있다. 알아야 할 사항은 다음과 같다.

우리는 종종 관계형 데이터베이스로 된 데이터를 접한다. 일반적으로 원시 SQL 쿼리를 작성하여 데이터베이스 엔진에 전달하고 일반 레코드 배열로 반환된 결과를 구문 분석하여 작업해야 한다.

SQLAlchemy는 이러한 데이터베이스와 상호 작용을 "파이썬" 방식으로 제공한다. MySQL, PostgreSQL 또는 Oracle과 같은 기존 SQL의 특정 언어 간의 차이점을 처리하는 대신 SQLAlchemy의 Python 프레임워크를 활용하여 워크플로우를 간소화하고 데이터를 보다 효율적으로 쿼리할 수 있다.

> Python SQLAlchemy란?<br>
> Python SQLAlchemy는 관계형 데이터베이스와 상호 작용을 Python 방식으로 사용자에게 제공하는 데이터베이스 툴킷이다. 이 프로그램을 사용하면 MySQL, PostgreSQL, Oracle과 같은 특정 SQL 언어 간의 차이점을 탐색할 필요 없이 Python으로 데이터 쿼리를 작성할 수 있으므로 워크플로우를 보다 효율적이고 간소화할 수 있다.

<a name="footnote_1">1</a>: 이 페이지는 [Python SQLAlchemy: A Tutorial](https://builtin.com/data-science/python-sqlalchemy)을 편역한 것이다.